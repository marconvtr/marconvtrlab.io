+++
author = "Victor Aguiar Marccon"
title = "First"
date = "2020-03-31"
draft = false
description = "Introduction to my webpage"
tags = ["intro", "blog"]
categories = ["about", "areas"]
images  = ["img/2020/board-march.jpg"]
aliases = ["intro-post"]
+++

This article is about me, what I want to do, and general introduction.
<!--more-->

# Hello world!


First I want to talk about me, my name is Victor Aguiar Marcon, i am an hardware/software enthusiast, currently I work as embedded software developer in Brazil, with heavy focus on IOT area.

I love microcontroller, wireless SoCs, Chinese clones, cheap interesting micros, embedded Linux, and general C language development and occasional C++ Python and other more modern languages, always open to learn something new that adds to my portfolio of skills

I am creating this blog inspired by great engineers around the world that have static pages with great material like [thirtythreeforty](https://www.thirtythreeforty.net) and so I decided maybe sometimes I have something nice to share too

This is a simple static web page created with [hugo.io](https://gohugo.io/) that made the process really straightforward. Quarantine is also a big help to put some projects out of the drawer...

So, in the next weeks I hope I have something nice to share, maybe I can start with some LCD displays and ESP32, maybe some low power tips, don't know, time will tell!

See ya.

 