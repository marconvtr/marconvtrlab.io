+++
title = "About Me"
date = "2014-04-09"
+++

I am an embedded software developer, currently living in Brazil, I have a degree in computer engineering from UFSC (Universidade Federal de Santa Catarina) with heavy emphasis on embedded and real time systems, from PIC series to various flavors of arm processors I am always messing around with micro controllers

Currently I am heavily focused on wireless low power solutions involving any of the technologies on top of 802.15.4, GSM and bit o LoRa.

I'm always open to new challenges, and dive into new tech!

When not doing what I do always, I am sometimes messing around with algorithms, reading about economics, cryptocurrency, 
