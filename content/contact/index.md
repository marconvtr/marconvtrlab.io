+++
title = "Contact"
type = "contact"
netlify = false
emailservice = "formspree.io/marconvtr@gmail.com"
contactname = "Your name"
contactemail = "Your Email"
contactsubject = "Subject"
contactmessage = "Your Message"
contactlang = "en"
contactanswertime = 24
+++

You can always contact me via my e-mail about doubts, opportunities and ideas for projects, I am always open to cool ideas!